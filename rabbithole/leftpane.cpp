//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <algorithm>
#include <map>
#include <string>
#include <vector>

#include <imgui.h>

#include "node/nodeinfo.h"
#include "node/noderegistry.h"
#include "leftpane.h"


namespace LeftPane
{

static const size_t sFilterBufferSize = 32;
static char sFilterBuffer[sFilterBufferSize];
static std::string sCurrentFilter;
static std::string sPreviousFilter;

using NodeCategoryMap = std::map<std::string, NodeInfos>;
static NodeCategoryMap sNodeCategoryMap;
static NodeCategoryMap sFilteredNodeCategoryMap;

//------------------------------------------------------------------------------
void Initialise()
{
	memset(sFilterBuffer, 0, sFilterBufferSize);

	const NodeInfos& nodeInfos = NodeRegistry::Get();
	for (NodeInfo* pNodeInfo : nodeInfos)
	{
		NodeInfos& categoryNodeInfos = sNodeCategoryMap[pNodeInfo->GetCategory()];
		categoryNodeInfos.push_back(pNodeInfo);
	}
}

//------------------------------------------------------------------------------
// Called back whenever the Filter input box is active.
// Performs case-insensitive filtering of the nodes displayed on this pane.
//------------------------------------------------------------------------------
int FilterInputCallback(ImGuiInputTextCallbackData* data)
{
	sCurrentFilter = std::string(data->Buf);
	std::transform(sCurrentFilter.begin(), sCurrentFilter.end(), sCurrentFilter.begin(), ::tolower);

	if (sCurrentFilter != sPreviousFilter)
	{
		sFilteredNodeCategoryMap.clear();

		if (sCurrentFilter.empty() == false)
		{		
			for (const auto& pair : sNodeCategoryMap)
			{
				for (NodeInfo* pNodeInfo : pair.second)
				{
					std::string loweredName(pNodeInfo->GetDisplayName());
					std::transform(loweredName.begin(), loweredName.end(), loweredName.begin(), ::tolower);

					if (loweredName.find(sCurrentFilter) != std::string::npos)
					{
						NodeInfos& filteredCategoryNodeInfos = sFilteredNodeCategoryMap[pNodeInfo->GetCategory()];
						filteredCategoryNodeInfos.push_back(pNodeInfo);
					}
				}
			}
		}

		sPreviousFilter = sCurrentFilter;
	}

	return 0;
}

//------------------------------------------------------------------------------
void Draw(float paneWidth)
{
	auto& io = ImGui::GetIO();

	ImGui::BeginChild("Selection", ImVec2(paneWidth, 0));

	paneWidth = ImGui::GetContentRegionAvailWidth();

	ImGui::BeginHorizontal("Style Editor", ImVec2(paneWidth, 0));
	ImGui::Spring(0.0f, 0.0f);
	ImGui::EndHorizontal();

	ImGui::InputText("Filter", sFilterBuffer, sFilterBufferSize, ImGuiInputTextFlags_CallbackAlways, &FilterInputCallback);
	ImGui::Dummy(ImVec2(4, 6));
	const float sz = ImGui::GetTextLineHeight();
	const bool filterActive = !sCurrentFilter.empty();
	NodeCategoryMap& nodeCategoryMap = filterActive ? sFilteredNodeCategoryMap : sNodeCategoryMap;

	for (auto& pair : nodeCategoryMap)
	{
		if (ImGui::CollapsingHeader(pair.first.c_str(), ImGuiTreeNodeFlags_DefaultOpen))
		{
			NodeInfos& nodeInfos = pair.second;
			for (NodeInfo* pNodeInfo : nodeInfos)
			{
				bool selected = false;
				ImGui::Dummy(ImVec2(4, sz));
				ImGui::SameLine();
				const char* name = pNodeInfo->GetDisplayName().c_str();
				ImGui::PushID(name);
				ImVec2 p = ImGui::GetCursorScreenPos();
				ImGui::Selectable("", &selected); ImGui::SameLine();

				if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
				{
					ImGui::SetDragDropPayload("DND_NODE", &pNodeInfo, sizeof(NodeInfo**));
					if (pNodeInfo->GetIcon() != nullptr)
					{
						ImGui::Image(pNodeInfo->GetIcon(), ImVec2(sz, sz));
						ImGui::SameLine();
					}
					ImGui::Text("%s", pNodeInfo->GetDisplayName().c_str());
					ImGui::EndDragDropSource();
				}

				if (pNodeInfo->GetIcon() != nullptr)
				{
					ImGui::GetWindowDrawList()->AddImage(pNodeInfo->GetIcon(), p, ImVec2(p.x + sz, p.y + sz));
					ImGui::SameLine();
				}
				ImGui::Dummy(ImVec2(sz, sz));
				ImGui::SameLine();
				ImGui::Text("%s", pNodeInfo->GetDisplayName().c_str());
				ImGui::PopID();
			}
		}
	}

	ImGui::EndChild();
}

}
