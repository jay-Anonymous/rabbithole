//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>

namespace RabbitHole {
namespace Import {

class ImportNode;
using ImportLinks = std::vector<ImportNode*>;

//------------------------------------------------------------------------------
// ImportNode
// An ImportNode contains the basic information required to create a real Node
// which can be edited. 
//------------------------------------------------------------------------------
class ImportNode
{
public:
	ImportNode(const std::string& internalName, const std::string& displayName, bool parentedToRoot);
	
	const std::string& GetInternalName() const;
	const std::string& GetDisplayName() const;
	const ImportLinks& GetLinks() const;
	void AddLink(ImportNode* pNode);
	bool IsParentedToRoot() const;

private:
	std::string m_InternalName;
	std::string m_DisplayName;
	ImportLinks m_Links;
	bool m_ParentedToRoot;
};

} // namespace Import
} // namespace RabbitHole
