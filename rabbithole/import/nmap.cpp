//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <tinyxml2.h>

#include "importnode.h"
#include "importresult.h"
#include "nmap.h"

#include "../node/node.h"
#include "../log.h"

using namespace tinyxml2;

namespace RabbitHole {
namespace Import {

//------------------------------------------------------------------------------
// Auxiliary structures used during parsing and node generation.
//------------------------------------------------------------------------------
struct Service
{
	std::string name;
	std::string product;
	std::string version;
};

struct Port
{
	std::string protocol;
	std::string id;
	std::shared_ptr<Service> service;
};

struct Host
{
	std::string address;
	std::vector<std::string> hostnames;
	std::vector<Port> ports;
};

using Hosts = std::vector<Host>;

//------------------------------------------------------------------------------
static std::string ReadXMLAttribute(XMLElement* pElem, const char* pAttr, const char* pDefault = "")
{
	if (pElem->Attribute(pAttr) != nullptr)
	{
		return pElem->Attribute(pAttr);
	}
	else
	{
		return pDefault;
	}}

//------------------------------------------------------------------------------
static Host LoadHost(XMLElement* pHostElem)
{
	Host host;

	for (XMLElement* pHostChildElem = pHostElem->FirstChildElement(); pHostChildElem != nullptr; pHostChildElem = pHostChildElem->NextSiblingElement())
	{
		std::string hostChildValue = pHostChildElem->Value();
		if (hostChildValue == "address")
		{
			host.address = ReadXMLAttribute(pHostChildElem, "addr");
		}
		else if (hostChildValue == "hostnames")
		{
			for (XMLElement* pHostnameChildElem = pHostChildElem->FirstChildElement(); pHostnameChildElem != nullptr; pHostnameChildElem = pHostnameChildElem->NextSiblingElement())
			{
				std::string hostnameChildValue = pHostnameChildElem->Value();
				if (hostnameChildValue == "hostname")
				{
					std::string hostname = ReadXMLAttribute(pHostnameChildElem, "name");
					if (hostname.empty() == false)
					{
						host.hostnames.push_back(hostname);
					}
				}
			}
		}
		else if (hostChildValue == "ports")
		{
			for (XMLElement* pPortChildElem = pHostChildElem->FirstChildElement(); pPortChildElem != nullptr; pPortChildElem = pPortChildElem->NextSiblingElement())
			{
				std::string portChildValue = pPortChildElem->Value();
				if (portChildValue == "port")
				{
					Port port;
					port.id = ReadXMLAttribute(pPortChildElem,"portid", "0");
					port.protocol = ReadXMLAttribute(pPortChildElem, "protocol", "???");
					port.service = nullptr;

					for (XMLElement* pServiceChildElem = pPortChildElem->FirstChildElement(); pServiceChildElem != nullptr; pServiceChildElem = pServiceChildElem->NextSiblingElement())
					{
						std::string serviceChildValue = pServiceChildElem->Value();
						if (serviceChildValue == "service" && port.service == nullptr)
						{
							port.service = std::make_shared<Service>();
							port.service->name = ReadXMLAttribute(pServiceChildElem, "name", "???"); 
							port.service->product = ReadXMLAttribute(pServiceChildElem, "product"); 
							port.service->version = ReadXMLAttribute(pServiceChildElem, "version");
						}
					}

					host.ports.push_back(port);
				}
			}
		}
	}

	return host;
}

//------------------------------------------------------------------------------
static void SanitiseXML(std::string& contents)
{	
	size_t contentsSize = contents.size();
	size_t pos = contents.find("<?xml-stylesheet");
	if (pos != std::string::npos)
	{
		size_t elementEnd = contents.find("?>", pos);
		if (elementEnd != std::string::npos)
		{
			for (size_t i = elementEnd; i < contentsSize; ++i)
			{
				contents[pos++] = contents[i];
			}
			contents.resize(pos);
		}
	}
}

//------------------------------------------------------------------------------
static bool ReadFileContents(const std::string& filename, std::string& contents)
{
	std::ifstream file;
	file.open(filename, std::ios::in);
	if (file.is_open())
	{
		file.seekg(0, std::ios::end);
		const std::size_t fileSize = file.tellg();
		contents.resize(fileSize);
		file.seekg(0, std::ios::beg);
		file.read(&contents[0], fileSize);
		file.close();
		return true;
	}
	else
	{
		return false;
	}
}

//------------------------------------------------------------------------------
static bool LoadXML(const std::string& filename, Hosts& hosts)
{
	std::string xmlContents;
	if (ReadFileContents(filename, xmlContents) == false)
	{
		Log::Warning("[Import|Nmap] Couldn't load file '%s'.", filename.c_str());
		return false;
	}

	SanitiseXML(xmlContents);

	XMLDocument doc;
	if (doc.Parse(xmlContents.data(), xmlContents.size()) == XML_SUCCESS)
	{
		XMLElement* pNmapRunElem = doc.FirstChildElement("nmaprun");
		if (pNmapRunElem == nullptr)
		{
			Log::Warning("[Import|Nmap] Couldn't find XML element 'nmaprun' in file '%s'.", filename.c_str());
			return false;
		}

		for (XMLElement* pChildElem = pNmapRunElem->FirstChildElement(); pChildElem != nullptr; pChildElem = pChildElem->NextSiblingElement()) 
		{
			std::string value = pChildElem->Value();
			if (value == "host")
			{
				hosts.push_back(LoadHost(pChildElem));
			}
		}
		
		Log::Info("[Import|Nmap] Found %d hosts.", hosts.size());
		
		return hosts.size() > 0u;
	}
	else
	{
		Log::Warning("[Import|Nmap] Couldn't load '%s': %s", filename.c_str(), doc.ErrorStr());
		return false;
	}
}

//------------------------------------------------------------------------------
static bool HostMatchesNode(const Host& host, Node* pNode)
{
	if (host.address == pNode->GetText())
	{
		return true;
	}

	for (const std::string& hostname : host.hostnames)
	{
		if (hostname == pNode->GetText())
		{
			return true;
		}
	}

	return false;
}

//------------------------------------------------------------------------------
std::string Nmap::GetName() const
{
	return "NMap XML output";
}

//------------------------------------------------------------------------------
bool Nmap::IsSupported(IImport::Context context) const
{
	return context == IImport::Context::Node;
}

//------------------------------------------------------------------------------
const std::vector<std::string> Nmap::GetSupportedNodes() const
{
	return std::vector<std::string>({ "rabbithole.ipaddress" });
}

//------------------------------------------------------------------------------
const std::vector<std::string> Nmap::GetExtensions() const
{
	return std::vector<std::string>({ ".xml" });
}

//------------------------------------------------------------------------------
ImportResult* Nmap::GlobalImport(const std::string& filename)
{
	return nullptr;
}

//------------------------------------------------------------------------------
ImportResult* Nmap::NodeImport(const std::string& filename, Node* pStartingNode)
{
	Hosts hosts;
	if (LoadXML(filename, hosts))
	{
		for (const Host& host : hosts)
		{
			if (HostMatchesNode(host, pStartingNode))
			{
				ImportResult* pResult = new ImportResult();
				pResult->SetSourceNode(pStartingNode);

				for (const Port& port : host.ports)
				{
					std::stringstream portText;
					portText << port.id << "/" << port.protocol;
					ImportNode* pPortNode = new ImportNode("rabbithole.port", portText.str(), true);
					pResult->AddNode(pPortNode);

					if (port.service != nullptr)
					{
						std::stringstream serviceText;
						if (port.service->product.empty())
						{
							serviceText << port.service->name;
						}
						else
						{
							serviceText << port.service->product;
						}

						if (port.service->version.empty() == false)
						{
							serviceText << " (" << port.service->version << ")";
						}

						ImportNode* pServiceNode = new ImportNode("rabbithole.service", serviceText.str(), false);
						pPortNode->AddLink(pServiceNode);
						pResult->AddNode(pServiceNode);
					}
				}

				return pResult;
			}
		}
	}

	return nullptr;
}

} // namespace Import
} // namespace RabbitHole
