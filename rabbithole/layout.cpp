#//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <utility>

#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/layered/MedianHeuristic.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/layered/OptimalRanking.h>
#include <ogdf/layered/SugiyamaLayout.h>

#include "node/node.h"
#include "layout.h"
#include "link.h"

using namespace ogdf;

namespace RabbitHole {
namespace Layout {

using LayoutNode = std::pair<Node*, ogdf::node>;

static bool sLayoutInProgress = false;

struct LayoutResult
{
	Node* pNode;
	ImVec2 initialPosition;
	ImVec2 finalPosition;
	bool updatedSize;
};
using LayoutResultVec = std::vector<LayoutResult>;
static LayoutResultVec sLayoutResults;
static float sInterpolation = 0.0f;

//------------------------------------------------------------------------------
// Internal use, shared by the public Perform() functions.
//------------------------------------------------------------------------------
bool Perform(const NodeVec& nodes, const LinkVec& links, Node* pRootNode, const ImVec2& rootPosition)
{
	if (sLayoutInProgress)
	{
		return false;
	}

	sLayoutInProgress = true;

	Graph graph;
	GraphAttributes GA(graph, GraphAttributes::nodeGraphics | GraphAttributes::edgeGraphics);

	std::vector<LayoutNode> layoutNodes;

	for (Node* pEditorNode : nodes)
	{
		layoutNodes.emplace_back(pEditorNode, graph.newNode());
	}

	auto FindLayoutNode = [&layoutNodes](ed::PinId pinId) -> LayoutNode*
	{	
		for (LayoutNode& layoutNode : layoutNodes)
		{
			if (layoutNode.first->GetPin().ID == pinId)
			{
				return &layoutNode;
			}
		}
		return nullptr;
	};

	for (const Link& link : links)
	{
		LayoutNode* pLayoutNodeA = FindLayoutNode(link.GetStartPinId());
		LayoutNode* pLayoutNodeB = FindLayoutNode(link.GetEndPinId());
		if (pLayoutNodeA != nullptr && pLayoutNodeB != nullptr)
		{
			graph.newEdge(pLayoutNodeA->second, pLayoutNodeB->second);
		}
	}

	SugiyamaLayout SL;
	SL.setRanking(new OptimalRanking);
	SL.setCrossMin(new MedianHeuristic);
	OptimalHierarchyLayout *ohl = new OptimalHierarchyLayout;
	ohl->layerDistance(150.0);
	ohl->nodeDistance(180.0);
	ohl->weightBalancing(0.8);
	SL.setLayout(ohl);
	SL.call(GA);

	ImVec2 editorAnchor = rootPosition;
	ImVec2 layoutOffset(0.0f, 0.0f);
	bool usingRootNode = (pRootNode != nullptr);
	if (usingRootNode)
	{
		editorAnchor = ed::GetNodePosition(pRootNode->GetId());
		editorAnchor.x += ed::GetNodeSize(pRootNode->GetId()).x / 2.0f;
		LayoutNode* pLayoutNode = FindLayoutNode(pRootNode->GetPin().ID);
		layoutOffset = ImVec2(static_cast<float>(GA.x(pLayoutNode->second)), static_cast<float>(GA.y(pLayoutNode->second)));
	}

	auto alignPointToGrid = [](float p) -> float
	{
		float a = p;
		float b = fmodf(p, 16.0f);

		return p - fmodf(p, 16.0f);
	};

	sLayoutResults.reserve(layoutNodes.size());
	for (LayoutNode& layoutNode : layoutNodes)
	{
		LayoutResult result;
		result.pNode = layoutNode.first;
		result.initialPosition = (layoutNode.first == pRootNode) ? ed::GetNodePosition(pRootNode->GetId()) : editorAnchor;
		result.finalPosition = result.initialPosition;
		result.finalPosition.x += static_cast<float>(GA.x(layoutNode.second)) - layoutOffset.x;
		result.finalPosition.y += static_cast<float>(GA.y(layoutNode.second)) - layoutOffset.y;
		result.updatedSize = (layoutNode.first == pRootNode);
		sLayoutResults.push_back(result);
	}

	sInterpolation = 0.0f;

	return true;
}

//------------------------------------------------------------------------------
static ImVec2 Lerp(const ImVec2& a, const ImVec2& b, float t)          
{ 
	return ImVec2(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t); 
}

//------------------------------------------------------------------------------
bool Perform(const NodeVec& nodes, const LinkVec& links, Node* pRootNode)
{
	return Perform(nodes, links, pRootNode, ImVec2(0, 0));
}

//------------------------------------------------------------------------------
bool Perform(const NodeVec& nodes, const LinkVec& links, const ImVec2& rootPosition)
{
	return Perform(nodes, links, nullptr, rootPosition);
}

//------------------------------------------------------------------------------
void Advance(float delta)
{
	if (sLayoutInProgress)
	{
		static const float sDuration = 0.5f;
		sInterpolation += delta / sDuration;
		bool finished = sInterpolation >= 1.0f;
		float interpolation = std::min(sInterpolation, 1.0f);

		for (LayoutResult& result : sLayoutResults)
		{
			if (result.updatedSize == false)
			{
				float nodeWidth = ed::GetNodeSize(result.pNode->GetId()).x;
				if (nodeWidth > std::numeric_limits<float>::epsilon())
				{
					result.finalPosition.x -= nodeWidth / 2.0f;
					result.updatedSize = true;
				}
			}

			if (result.updatedSize)
			{
				ed::SetNodePosition(result.pNode->GetId(), Lerp(result.initialPosition, result.finalPosition, interpolation));
			}
		}

		if (finished)
		{
			sLayoutResults.clear();
			sLayoutInProgress = false;
		}
	}
}

} // namespace Layout
} // namespace RabbitHole
