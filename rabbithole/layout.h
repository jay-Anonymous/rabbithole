//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <vector>

#include <imgui.h>

#include "link.h"

class Node;

using NodeVec = std::vector<Node*>;
using LinkVec = std::vector<Link>;

namespace RabbitHole
{
namespace Layout
{

// Do the layout operation, using a given root node as an anchor for the
// result. This anchor node must be part of the "nodes" vector.
// Will fail if there is already a layout operation in progress.
bool Perform(const NodeVec& nodes, const LinkVec& links, Node* pRootNode);

// Do the layout operation using a position as an anchor for the result, rather
// than a specific node.
// Will fail if there is already a layout operation in progress.
bool Perform(const NodeVec& nodes, const LinkVec& links, const ImVec2& rootPosition);

// Process an in-progress layout operation. Must be called every frame.
void Advance(float delta);

} // namespace Layout
} // namespace RabbitHole
