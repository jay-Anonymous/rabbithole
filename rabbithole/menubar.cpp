//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <imgui.h>
#include <imgui_file_dlg.h>

#include "menubar.h"
#include "rabbithole.h"

namespace Menubar
{

static const char* sExtension = ".rh";
static bool sOpenPending = false;
static bool sSavePending = false;

//------------------------------------------------------------------------------
void ShowOpenWindow();
void ShowSaveWindow();

//------------------------------------------------------------------------------
void Draw()
{
	if (ImGui::BeginMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("New"))
			{
				RabbitHole::NewFile();
			}

			if (ImGui::MenuItem("Open"))
			{
				sOpenPending = true;
			}

			if (ImGui::MenuItem("Save"))
			{
				sSavePending = true;
			}

			if (ImGui::MenuItem("Exit"))
			{
				RabbitHole::Quit();
			}

			ImGui::EndMenu();
		}

		ImGui::EndMenuBar();
	}

	ShowOpenWindow();
	ShowSaveWindow();
}

//------------------------------------------------------------------------------
void ShowOpenWindow()
{
	if (sOpenPending == false)
	{
		return;
	}

	if (ImGui::FileDialog("Open file", { sExtension }))
	{
		if (ImGui::FileDialogAccepted() && ImGui::FileDialogFilename().empty() == false)
		{
			RabbitHole::OpenFile(ImGui::FileDialogFullPath());
			ImGui::FileDialogReset();
		}

		sOpenPending = false;
	}
}

//------------------------------------------------------------------------------
void ShowSaveWindow()
{
	if (sSavePending == false)
	{
		return;
	}

	if (ImGui::FileDialog("Save file", { sExtension }))
	{
		if (ImGui::FileDialogAccepted() && ImGui::FileDialogFilename().empty() == false)
		{
			std::string file = ImGui::FileDialogFullPath();

			// Ensure the extension is present in the final filename.
			if (file.rfind(sExtension) == std::string::npos)
			{
				file += sExtension;
			}

			RabbitHole::SaveFile(file);
			ImGui::FileDialogReset();
		}

		sSavePending = false;
	}
}

} // namespace Menubar
