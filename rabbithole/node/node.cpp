//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <imgui_node_editor.h>

#include "node.h"
#include "nodeinfo.h"
#include "nodefactory.h"
#include "noderegistry.h"
#include "../idgenerator.h"

using json = nlohmann::json;
namespace ed = ax::NodeEditor;

//------------------------------------------------------------------------------
Node::Node() :
m_Id(IdGenerator::InvalidId),
m_pNodeInfo(nullptr),
m_Pin(IdGenerator::InvalidId, "", PinType::Flow)
{
	m_Pin.Node = this;
	m_Pin.Kind = PinKind::Input;
}

//------------------------------------------------------------------------------
Node::Node(NodeInfo* pNodeInfo) :
m_Id(IdGenerator::Next()),
m_pNodeInfo(pNodeInfo),
m_Pin(IdGenerator::Next(), "", PinType::Flow)
{
	SetText(m_pNodeInfo->GetDefaultText());

	m_Pin.Node = this;
	m_Pin.Kind = PinKind::Input;
}

//------------------------------------------------------------------------------
const ed::NodeId& Node::GetId() const
{
	return m_Id;
}

//------------------------------------------------------------------------------
const std::string& Node::GetText() const
{
	return m_Text;
}

//------------------------------------------------------------------------------
void Node::SetText(const std::string& text)
{
	m_Text = text;
}

//------------------------------------------------------------------------------
void* Node::GetIcon() const
{
	return m_pNodeInfo->GetIcon();
}

//------------------------------------------------------------------------------
NodeInfo* Node::GetNodeInfo() const
{
	return m_pNodeInfo;
}

//------------------------------------------------------------------------------
const Pin& Node::GetPin() const
{
	return m_Pin;
}

//------------------------------------------------------------------------------
Pin& Node::GetPin()
{
	return m_Pin;
}

//------------------------------------------------------------------------------
json Node::Serialise() const
{
	ImVec2 position = ed::GetNodePosition(m_Id);

	json j = {
		{ "id", static_cast<unsigned long>(m_Id.Get()) },
		{ "pin_id", static_cast<unsigned long>(m_Pin.ID.Get()) },
		{ "x", position.x },
		{ "y", position.y },
		{ "internal_name", m_pNodeInfo->GetInternalName() },
		{ "text", m_Text }
	};
	return j;
}

//------------------------------------------------------------------------------
bool Node::Deserialise(const json& object)
{
	// TODO: Make deserialisation code more robust.
	unsigned int id = object["id"].get<unsigned int>();
	IdGenerator::Register(id);
	m_Id = id;
	unsigned int pinId = object["pin_id"].get<unsigned int>();
	IdGenerator::Register(pinId);
	m_Pin.ID = pinId;
	float x = object["x"].get<float>();
	float y = object["y"].get<float>();
	ed::SetNodePosition(m_Id, ImVec2(x, y));
	m_Text = object["text"].get<std::string>();
	std::string internalName = object["internal_name"].get<std::string>();
	m_pNodeInfo = NodeRegistry::Find(internalName);

	return m_pNodeInfo != nullptr;
}
