//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include "nodeeditwindow.h"
#include "nodeinfo.h"
#include "node.h"

//------------------------------------------------------------------------------
NodeEditWindow::NodeEditWindow(Node* pNode, const ImVec2& position) :
	m_pNode(pNode),
	m_StartingPosition(position),
	m_StartingPositionSet(false),
	m_IsOpen(true)
{
	memset(m_Buffer, 0, sBufferSize);
}

//------------------------------------------------------------------------------
bool NodeEditWindow::operator==(const NodeEditWindow& window)
{
	return m_pNode->GetId() == window.GetNode()->GetId();
}

//------------------------------------------------------------------------------
void NodeEditWindow::Draw()
{
	if (m_StartingPositionSet == false)
	{
		ImGui::SetNextWindowPos(m_StartingPosition);
		ImGui::SetNextWindowSize(ImVec2(400, 80));
		m_StartingPositionSet = true;
	}

	ImGuiWindowFlags window_flags = 0;
	std::string titleBar(m_pNode->GetNodeInfo()->GetDisplayName() + "##" + std::to_string(reinterpret_cast<uintptr_t>(m_pNode->GetId().AsPointer())));
	if (!ImGui::Begin(titleBar.c_str(), &m_IsOpen, window_flags))
	{
		// Early out if the window is collapsed, as an optimization.
		ImGui::End();
		return;
	}

#ifdef _WIN32
	strncpy_s(m_Buffer, sBufferSize, m_pNode->GetText().c_str(), sBufferSize);
#else
	strncpy(m_Buffer, m_pNode->GetText().c_str(), sBufferSize);
#endif

	ImGui::InputText("Name", m_Buffer, sBufferSize);
	
	m_pNode->SetText(m_Buffer);

	ImGui::End();
}

//------------------------------------------------------------------------------
Node* NodeEditWindow::GetNode() const
{
	return m_pNode;
}

//------------------------------------------------------------------------------
bool NodeEditWindow::IsOpen() const
{
	return m_IsOpen;
}
