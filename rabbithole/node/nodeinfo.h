//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <string>

//------------------------------------------------------------------------------
// NodeInfo
// The basic information necessary to create a new Node.
// It also loads the icon, in order to avoid multiple copies of the same data.
//------------------------------------------------------------------------------
class NodeInfo
{
public:
	NodeInfo(const std::string& internalName, const std::string& displayName, const std::string& defaultText, const std::string& category, const std::string& icon);
	~NodeInfo();
	const std::string& GetInternalName() const;
	const std::string& GetDisplayName() const;
	const std::string& GetDefaultText() const;
	const std::string& GetCategory() const;
	void* GetIcon() const;

private:
	std::string m_InternalName;
	std::string m_DisplayName;
	std::string m_DefaultText;
	std::string m_Category;
	void* m_pIcon;
};
