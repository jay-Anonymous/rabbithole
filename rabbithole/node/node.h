#pragma once

#include <string>

#include <imgui_node_editor.h>

#include "../iserialisable.h"

namespace ed = ax::NodeEditor;

class Node;
class NodeInfo;

enum class PinType
{
	Flow,
	Bool,
	Int,
	Float,
	String,
	Object,
	Function,
	Delegate,
};

enum class PinKind
{
	Output,
	Input
};

struct Pin
{
	ed::PinId   ID;
	::Node*     Node;
	std::string Name;
	PinType     Type;
	PinKind     Kind;

	Pin(int id, const char* name, PinType type) :
		ID(id), Node(nullptr), Name(name), Type(type), Kind(PinKind::Input)
	{
	}
};

class Node : public ISerialisable
{
public:
	Node();
	Node(NodeInfo* pNodeInfo);

	const ed::NodeId& GetId() const;
	const std::string& GetText() const;
	void SetText(const std::string& text);
	void* GetIcon() const;
	NodeInfo* GetNodeInfo() const;
	const Pin& GetPin() const;
	Pin& GetPin();

	// From ISerialisable:
	virtual nlohmann::json Serialise() const override;
	virtual bool Deserialise(const nlohmann::json& object) override;

private:
	ed::NodeId m_Id;
	NodeInfo* m_pNodeInfo;
	std::string m_Text;
	Pin m_Pin;
};
