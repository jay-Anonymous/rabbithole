//------------------------------------------------------------------------------
// This file is part of Rabbit Hole.
//
// Rabbit Hole is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rabbit Hole is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rabbit Hole. If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#pragma once

#include <string>
#include <vector>

class NodeInfo;
using NodeInfos = std::vector<NodeInfo*>;

//------------------------------------------------------------------------------
// NodeRegistry
// Accessor for the node information (represented in a NodeInfo) of all the 
// nodes which can be created inside Rabbit Hole.
//------------------------------------------------------------------------------
class NodeRegistry
{
public:
	static void Initialise();
	static void Shutdown();
	static NodeInfo* Find(const std::string& internalName);
	static const NodeInfos& Get(); 
};
