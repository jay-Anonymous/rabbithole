
if (TARGET OGDF)
    return()
endif()

set(_ogdf_SourceDir ${CMAKE_SOURCE_DIR}/ext/ogdf)
set(_ogdf_BinaryDir ${CMAKE_BINARY_DIR}/ext/ogdf)

add_subdirectory(${_ogdf_SourceDir} ${_ogdf_BinaryDir})

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

find_package_handle_standard_args(
    OGDF
    REQUIRED_VARS
        _ogdf_SourceDir
)

