
if (TARGET application)
    return()
endif()

set(_application_SourceDir ${CMAKE_SOURCE_DIR}/application)
set(_application_BinaryDir ${CMAKE_BINARY_DIR}/application)

add_subdirectory(${_application_SourceDir} ${_application_BinaryDir})

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

find_package_handle_standard_args(
    application
    REQUIRED_VARS
        _application_SourceDir
)

